import { SQLite, SQLiteObject } from "@awesome-cordova-plugins/sqlite/ngx";
import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  nama_usaha!: string;
  password: string = '';

  constructor(
    public navCtrl: NavController,
    public sqlite: SQLite,
    public storage: Storage,
  ) { }

  ngOnInit() {
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      let query: any = 'SELECT * FROM akun';
      db.executeSql(query, []).then(async (data) => {
        this.nama_usaha = data.rows.item(0).nama_usaha;
      }).catch(e => console.error(e));
    }).catch(e => console.error(e));
  }

  login() {
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      let query: any = 'SELECT * FROM akun';
      db.executeSql(query, []).then(async (data) => {
        if (!this.password) {
          alert("Silahkan isi password.");
        } else if (this.password != data.rows.item(0).password) {
          alert("Password salah.")
        } else {
          this.storage.set('loggedin', true);
          this.navCtrl.navigateRoot('/produk');
        }
      }).catch(e => console.error(e));
    }).catch(e => console.error(e));
  }
}
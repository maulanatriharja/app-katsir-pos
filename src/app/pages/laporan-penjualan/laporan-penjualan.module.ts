import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LaporanPenjualanPageRoutingModule } from './laporan-penjualan-routing.module';

import { LaporanPenjualanPage } from './laporan-penjualan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LaporanPenjualanPageRoutingModule
  ],
  declarations: [LaporanPenjualanPage]
})
export class LaporanPenjualanPageModule {}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LaporanPenjualanPage } from './laporan-penjualan.page';

const routes: Routes = [
  {
    path: '',
    component: LaporanPenjualanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LaporanPenjualanPageRoutingModule {}

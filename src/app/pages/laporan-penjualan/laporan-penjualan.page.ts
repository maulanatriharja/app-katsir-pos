import { SQLite, SQLiteObject } from "@awesome-cordova-plugins/sqlite/ngx";
import { Component, OnInit } from '@angular/core';

import * as moment from 'moment';

@Component({
  selector: 'app-laporan-penjualan',
  templateUrl: './laporan-penjualan.page.html',
  styleUrls: ['./laporan-penjualan.page.scss'],
})
export class LaporanPenjualanPage implements OnInit {

  data: any = [];
  empty: boolean = true;

  bulan: any = moment().format('YYYY-MM-DD');

  grand_pokok: number = 0;
  grand_jual: number = 0;
  grand_laba: number = 0;

  constructor(
    public sqlite: SQLite,
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter() {


    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      let query: any = 'SELECT * '
        + "FROM transaksi_global AS t1 "
        + "LEFT JOIN (SELECT nota, SUM(harga_pokok*kuantiti) AS total_pokok, SUM(harga_jual*kuantiti) AS total_jual, (SUM(harga_jual*kuantiti) - SUM(harga_pokok*kuantiti)) AS total_laba FROM transaksi_detail GROUP BY nota) AS t2 ON t1.nota=t2.nota "
        + "WHERE strftime('%m', tanggal) = '" + this.bulan.substr(5, 2) + "' AND strftime('%Y', tanggal) = '" + this.bulan.substr(0, 4) + "'";
      db.executeSql(query, []).then((data) => {
        console.log('list data showed');

        this.data = [];
        this.grand_pokok = 0;
        this.grand_jual = 0;
        this.grand_laba = 0;

        if (data.rows.length > 0) {
          this.empty = false;

          for (let i = 0; i < data.rows.length; i++) {
            this.data.push(data.rows.item(i));

            this.grand_pokok += data.rows.item(i).total_pokok;
            this.grand_jual += data.rows.item(i).total_jual;
            this.grand_laba += data.rows.item(i).total_laba;
          }
        } else {
          this.empty = true;
        }
      }).catch(e => console.error(e));
    }).catch(e => console.error(e));
  }
}
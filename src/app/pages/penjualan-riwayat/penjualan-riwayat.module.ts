import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PenjualanRiwayatPageRoutingModule } from './penjualan-riwayat-routing.module';

import { PenjualanRiwayatPage } from './penjualan-riwayat.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PenjualanRiwayatPageRoutingModule
  ],
  declarations: [PenjualanRiwayatPage]
})
export class PenjualanRiwayatPageModule {}

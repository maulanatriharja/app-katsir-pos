import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PenjualanRiwayatPage } from './penjualan-riwayat.page';

const routes: Routes = [
  {
    path: '',
    component: PenjualanRiwayatPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PenjualanRiwayatPageRoutingModule {}

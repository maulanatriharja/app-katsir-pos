import { SQLite, SQLiteObject } from "@awesome-cordova-plugins/sqlite/ngx";
import { Component, OnInit } from '@angular/core';

import * as moment from 'moment';

@Component({
  selector: 'app-penjualan-riwayat',
  templateUrl: './penjualan-riwayat.page.html',
  styleUrls: ['./penjualan-riwayat.page.scss'],
})
export class PenjualanRiwayatPage implements OnInit {

  data: any = [];
  empty: boolean = true;

  bulan: any = moment().format('YYYY-MM-DD');

  grandtotal: number = 0;

  constructor(
    public sqlite: SQLite,
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter() {


    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      let query: any = 'SELECT * '
        + "FROM transaksi_global AS t1 "
        + "LEFT JOIN (SELECT nota, SUM(harga_jual*kuantiti) AS total FROM transaksi_detail GROUP BY nota) AS t2 ON t1.nota=t2.nota "
        + "WHERE strftime('%m', tanggal) = '" + this.bulan.substr(5, 2) + "' AND strftime('%Y', tanggal) = '" + this.bulan.substr(0, 4) + "'";
      db.executeSql(query, []).then((data) => {
        console.log('list data showed');

        this.data = [];
        this.grandtotal = 0;

        if (data.rows.length > 0) {
          this.empty = false;

          for (let i = 0; i < data.rows.length; i++) {
            this.data.push(data.rows.item(i));

            this.grandtotal += data.rows.item(i).total;
          }
        } else {
          this.empty = true;
        }
      }).catch(e => console.error(e));
    }).catch(e => console.error(e));
  }
}
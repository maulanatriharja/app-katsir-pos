import { Component, OnInit } from '@angular/core';
import { AppVersion } from '@awesome-cordova-plugins/app-version/ngx';

@Component({
  selector: 'app-info',
  templateUrl: './info.page.html',
  styleUrls: ['./info.page.scss'],
})
export class InfoPage implements OnInit {

  versi!: string;

  constructor(
    public appVersion: AppVersion
  ) { }

  ngOnInit() {

    this.appVersion.getVersionNumber().then((val) => {
      this.versi = val;
    });
  }


}

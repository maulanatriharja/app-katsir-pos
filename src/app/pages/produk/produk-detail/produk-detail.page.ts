import { ActionSheetController, AlertController, ToastController } from '@ionic/angular';
import { SQLite, SQLiteObject } from "@awesome-cordova-plugins/sqlite/ngx";
import { FormGroup, FormBuilder } from "@angular/forms";
import { AdMobPro } from '@awesome-cordova-plugins/admob-pro/ngx';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from "@angular/common";
import { Camera, CameraResultType, CameraSource } from '@capacitor/camera';
import { FilePath } from '@awesome-cordova-plugins/file-path/ngx';

import * as moment from 'moment';

@Component({
  selector: 'app-produk-detail',
  templateUrl: './produk-detail.page.html',
  styleUrls: ['./produk-detail.page.scss'],
})
export class ProdukDetailPage implements OnInit {

  id: string = this.route.snapshot.paramMap.get('id')!;

  foto_path: any;
  foto_url!: string;
  foto_old!: string;
  win: any = window;

  myForm: FormGroup;
  isSubmitted = false;

  constructor(
    public actionSheetController: ActionSheetController,
    public admob: AdMobPro,
    public alertController: AlertController,
    // public camera: Camera,
    public filePath: FilePath,
    public formBuilder: FormBuilder,
    public location: Location,
    public route: ActivatedRoute,
    public sqlite: SQLite,
    public toastController: ToastController,
  ) {
    this.myForm = this.formBuilder.group({
      nama_produk: [''],
      harga_pokok: [''],
      harga_jual: [''],
      stok: [''],
      keterangan: [''],
    });
  }

  ngOnInit() {
    if (this.id) {
      this.sqlite.create({
        name: 'data.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        let query: any = "SELECT * FROM produk WHERE id=?";
        db.executeSql(query, [this.id]).then((data) => {
          console.info(JSON.stringify(data));

          this.myForm = this.formBuilder.group({
            nama_produk: [data.rows.item(0).nama_produk],
            harga_pokok: [data.rows.item(0).harga_pokok],
            harga_jual: [data.rows.item(0).harga_jual],
            stok: [data.rows.item(0).stok],
            keterangan: [data.rows.item(0).keterangan],
          });

          this.foto_url = data.rows.item(0).foto;

        }).catch(e => console.error(e));
      }).catch(e => console.error(e));
    }
  }

  inputFocus() {
    this.admob.hideBanner();
  }

  async ubah_foto() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Update Photo',
      buttons: [{
        text: 'Kamera',
        icon: 'camera',
        handler: () => {
          console.info('Open Camera');
          this.openCamera();
        }
      }, {
        text: 'Galeri',
        icon: 'images',
        handler: () => {
          console.info('Open Gallery');
          this.openGallery();
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  async openCamera() {
    // let options = {
    //   // allowEdit: true,
    //   correctOrientation: true,
    //   encodingType: this.camera.EncodingType.PNG,
    //   quality: 90,
    //   saveToPhotoAlbum: true,
    //   sourceType: this.camera.PictureSourceType.CAMERA,
    //   targetHeight: 400,
    //   targetWidth: 400,
    // };

    // this.camera.getPicture(options).then((imagePath) => {
    //   this.filePath.resolveNativePath(imagePath).then(async filePath => {
    //     this.foto_path = filePath;
    //     this.foto_url = this.win.Ionic.WebView.convertFileSrc(filePath);;
    //   });
    // }, (err) => {
    //   // alert('Error while selecting image.');
    //   alert(err);
    //   //this.alertCtrl.create({subTitle: 'ERR : ' + error + ' ' + JSON.stringify(error), buttons: ['OK']}).present();
    // });

    //===


    // const takePicture = async () => {
    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: true,
      resultType: CameraResultType.Uri,
      width: 600,
      correctOrientation: true,
      source: CameraSource.Camera,

    });
    var imageUrl = image.webPath;

    this.foto_path = imageUrl;
    this.foto_url = this.win.Ionic.WebView.convertFileSrc(imageUrl);;
    // };
  }

  async openGallery() {
    // let options = {
    //   // allowEdit: true,
    //   correctOrientation: true,
    //   encodingType: this.camera.EncodingType.PNG,
    //   quality: 90,
    //   saveToPhotoAlbum: true,
    //   sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    //   targetHeight: 400,
    //   targetWidth: 400,
    // };

    // this.camera.getPicture(options).then((imagePath) => {
    //   this.filePath.resolveNativePath(imagePath).then(async filePath => {
    //     this.foto_path = filePath;
    //     this.foto_url = this.win.Ionic.WebView.convertFileSrc(filePath) + '?time=' + moment();
    //   });
    // }, (err) => {
    //   // alert('Error while selecting image.');
    //   alert(err);
    //   //this.alertCtrl.create({subTitle: 'ERR : ' + error + ' ' + JSON.stringify(error), buttons: ['OK']}).present();
    // });

    //===


    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: false,
      resultType: CameraResultType.Uri,
      width: 600,
      correctOrientation: true,
      source: CameraSource.Photos,

    });
    var imageUrl = image.webPath;

    this.foto_path = imageUrl;
    this.foto_url = this.win.Ionic.WebView.convertFileSrc(imageUrl);;
  }

  submit() {
    this.isSubmitted = true;

    if (this.myForm.valid) {
      if (this.id) {
        this.update();
      } else {
        this.simpan();
      }
    }
  }

  simpan() {
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      let query: any = 'INSERT INTO produk ( nama_produk, harga_pokok, harga_jual, stok, keterangan, foto ) VALUES ( ?, ?, ?, ?, ?, ?)';
      db.executeSql(query, [
        this.myForm.get('nama_produk')!.value,
        this.myForm.get('harga_pokok')!.value,
        this.myForm.get('harga_jual')!.value,
        this.myForm.get('stok')!.value * 1,
        this.myForm.get('keterangan')!.value,
        this.foto_url
      ]).then(async () => {
        console.log('new data inserted');

        const toast = await this.toastController.create({
          message: 'Berhasil menambah data baru.',
          position: 'top',
          color: 'success',
          duration: 2000,
          buttons: [
            {
              text: 'X',
              role: 'cancel',
              handler: () => {
                console.log('Cancel clicked');
              }
            }
          ]
        });
        toast.present();

        this.location.back();
      }).catch(e => console.error(e));
    }).catch(e => console.error(e));
  }

  update() {
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      let query: any = 'UPDATE produk SET nama_produk=?, harga_pokok=?, harga_jual=?, stok=?, keterangan=?, foto=? WHERE id=?';
      db.executeSql(query, [
        this.myForm.get('nama_produk')!.value,
        this.myForm.get('harga_pokok')!.value,
        this.myForm.get('harga_jual')!.value,
        this.myForm.get('stok')!.value * 1,
        this.myForm.get('keterangan')!.value,
        this.foto_url,
        this.id
      ]).then(async () => {
        console.log('data updated');

        const toast = await this.toastController.create({
          message: 'Berhasil mengubah data.',
          position: 'top',
          color: 'success',
          duration: 2000,
          buttons: [
            {
              text: 'X',
              role: 'cancel',
              handler: () => {
                console.log('Cancel clicked');
              }
            }
          ]
        });
        toast.present();

        this.location.back();
      }).catch(e => console.error(e));
    }).catch(e => console.error(e));
  }

  async hapus() {
    const alert = await this.alertController.create({
      header: 'Konfirmasi',
      message: 'Yakin untuk menghapus data ?',
      buttons: [
        {
          text: 'Batal',
          role: 'cancel',
        }, {
          text: 'Hapus',
          handler: () => {
            this.sqlite.create({
              name: 'data.db',
              location: 'default'
            }).then((db: SQLiteObject) => {
              let query: any = 'DELETE FROM produk WHERE id=?';
              db.executeSql(query, [this.id]).then(async () => {
                console.log('data deleted');

                const toast = await this.toastController.create({
                  message: 'Berhasil menghapus data.',
                  position: 'top',
                  color: 'medium',
                  duration: 2000,
                  buttons: [
                    {
                      text: 'X',
                      role: 'cancel',
                      handler: () => {
                        console.log('Cancel clicked');
                      }
                    }
                  ]
                });
                toast.present();

                this.location.back();
              }).catch(e => console.error(e));
            }).catch(e => console.error(e));
          }
        }
      ]
    });
    await alert.present();
  }

  ionViewDidLeave() {
    this.admob.showBanner(this.admob.AD_POSITION.BOTTOM_CENTER);
  }
}

import { SQLite, SQLiteObject } from "@awesome-cordova-plugins/sqlite/ngx";
import { AdMobPro } from '@awesome-cordova-plugins/admob-pro/ngx';
import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-produk',
  templateUrl: './produk.page.html',
  styleUrls: ['./produk.page.scss'],
})
export class ProdukPage implements OnInit {

  keyboardShown: boolean = false;

  data: any = [];
  empty: boolean = true;

  pencarian: any;

  constructor(
    public admob: AdMobPro,
    public platform: Platform,
    public sqlite: SQLite,
  ) {
  }

  ngOnInit() {
    this.platform.ready().then(() => {
      let options = {
        adId: 'ca-app-pub-3302992149561172/4638643072',
        isTesting: false,
      }
      this.admob.createBanner(options).then(() => { this.admob.showBanner; });

      this.sqlite.create({
        name: 'data.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        let query = 'CREATE TABLE IF NOT EXISTS produk( id INTEGER PRIMARY KEY, nama_produk, harga_pokok, harga_jual, stok, keterangan, foto )';
        db.executeSql(query, []).then(() => {
          console.log('new table produk created');
        }).catch(e => console.error(e));
      }).catch(e => console.error(e));
    });
  }

  inputFocus() {
    this.keyboardShown = true;
    this.admob.hideBanner();
  }

  inputBlur() {
    this.keyboardShown = false;
    this.admob.showBanner(this.admob.AD_POSITION.BOTTOM_CENTER);
  }

  ionViewDidEnter() {
    this.platform.ready().then(() => {

      if (!this.pencarian) { this.pencarian = ''; }

      console.info('pencarian :', this.pencarian)

      this.sqlite.create({
        name: 'data.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        let query: any = 'SELECT * FROM produk WHERE nama_produk LIKE ?';
        // db.executeSql(query, ['%' + event.target.value + '%']).then((data) => {
        db.executeSql(query, ['%' + this.pencarian + '%']).then((data) => {
          console.log('list data showed');

          this.data = [];

          if (data.rows.length > 0) {
            this.empty = false;

            for (let i = 0; i < data.rows.length; i++) {
              this.data.push(data.rows.item(i));
            }
          } else {
            this.empty = true;
          }

          console.info(JSON.stringify(this.data));
        }).catch(e => console.error(e));
      }).catch(e => console.error(e));
    });
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PenjualanCreatePage } from './penjualan-create.page';

const routes: Routes = [
  {
    path: '',
    component: PenjualanCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PenjualanCreatePageRoutingModule {}

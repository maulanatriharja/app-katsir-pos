import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PenjualanCreatePageRoutingModule } from './penjualan-create-routing.module';

import { PenjualanCreatePage } from './penjualan-create.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PenjualanCreatePageRoutingModule
  ],
  declarations: [PenjualanCreatePage]
})
export class PenjualanCreatePageModule {}

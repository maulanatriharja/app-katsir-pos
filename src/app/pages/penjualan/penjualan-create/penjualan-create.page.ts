import { AlertController, IonContent, ModalController, Platform, ToastController } from '@ionic/angular';
import { SQLite, SQLiteObject } from "@awesome-cordova-plugins/sqlite/ngx";
import { Component, OnInit, ViewChild } from '@angular/core';
import { FileOpener } from '@awesome-cordova-plugins/file-opener/ngx';
import { AdMobPro } from '@awesome-cordova-plugins/admob-pro/ngx';
import { ActivatedRoute } from '@angular/router';
import { File } from '@awesome-cordova-plugins/file/ngx';
import { Location } from "@angular/common";
import { Router } from '@angular/router';

import * as moment from 'moment';

import { PenjualanProdukPage } from '../penjualan-produk/penjualan-produk.page';

import * as pdfMake from "pdfmake/build/pdfmake";
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
(<any>pdfMake).vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-penjualan-create',
  templateUrl: './penjualan-create.page.html',
  styleUrls: ['./penjualan-create.page.scss'],
})
export class PenjualanCreatePage implements OnInit {
  @ViewChild(IonContent) content!: IonContent;

  data: any = [];
  total: number = 0;
  pembayaran!: number;
  kembalian: number = 0;
  empty: boolean = true;

  pdfObj: any = null;

  constructor(
    public admob: AdMobPro,
    public alertController: AlertController,
    public file: File,
    public fileOpener: FileOpener,
    public location: Location,
    public modalController: ModalController,
    public platform: Platform,
    public route: ActivatedRoute,
    public router: Router,
    public sqlite: SQLite,
    public toastController: ToastController,
  ) { }

  nota: any;
  tanggal!: string;
  jam!: string;
  nama_pelanggan!: string;

  ngOnInit() {
    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'data.db',
        location: 'default'
      }).then((db: SQLiteObject) => {

        let query_keranjang = 'CREATE TABLE IF NOT EXISTS keranjang( id INTEGER PRIMARY KEY, id_produk, harga_pokok, harga_jual, kuantiti)';
        db.executeSql(query_keranjang, []).then(() => { console.log('new cart table created'); }).catch(e => console.error(e));

        let query_transaksi_global = 'CREATE TABLE IF NOT EXISTS transaksi_global( id INTEGER PRIMARY KEY, nota, tanggal, jam, nama_pelanggan, pembayaran)';
        db.executeSql(query_transaksi_global, []).then(() => {
          console.log('new transaction table created');
        }).catch(e => console.error(e));

        let query_transaksi_detail = 'CREATE TABLE IF NOT EXISTS transaksi_detail( id INTEGER PRIMARY KEY, nota, id_produk, harga_pokok, harga_jual, kuantiti)';
        db.executeSql(query_transaksi_detail, []).then(() => { console.log('new detail transaction table created'); }).catch(e => console.error(e));

      }).catch(e => console.error(e));
    });
  }

  inputFocus() {
    this.admob.hideBanner();
  }

  inputBlur() {
    this.admob.showBanner(this.admob.AD_POSITION.BOTTOM_CENTER);
  }

  ionViewWillEnter() {

  }

  ionViewDidEnter() {
    this.platform.ready().then(() => {
      this.startOver();
      this.load_items();
    });
  }

  startOver() {
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      if (this.route.snapshot.paramMap.get('id')) {
        this.nota = this.route.snapshot.paramMap.get('id');
        let s = this.nota.length + 1 + "";
        while (s.length < 6) s = "0" + s;
        this.nota = s;
      } else {
        let query_nota: any = 'SELECT nota FROM transaksi_global';
        db.executeSql(query_nota, []).then((data) => {
          let s = data.rows.length + 1 + "";
          while (s.length < 6) s = "0" + s;
          this.nota = s;
        }).catch(e => console.error(e));
      }
    }).catch(e => console.error(e));

    this.tanggal = moment().format('YYYY-MM-DD');
    this.jam = moment().format('HH:mm');
    this.nama_pelanggan = '';
    // this.pembayaran = null;
    this.pembayaran = 0;
    this.total = 0;
    this.kembalian = 0;

    this.content.scrollToTop(300);
  }

  load_items() {
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      let query: any = 'SELECT t1.*, t2.nama_produk FROM keranjang t1 LEFT JOIN produk t2 ON t1.id_produk = t2.id';
      db.executeSql(query, []).then((data) => {
        console.log('list data showed');

        this.data = [];
        this.total = 0;

        if (data.rows.length > 0) {
          this.empty = false;

          for (let i = 0; i < data.rows.length; i++) {
            this.data.push(data.rows.item(i));

            this.total += (data.rows.item(i).harga_jual * data.rows.item(i).kuantiti);
          }

          this.change()
        } else {
          this.empty = true;
        }
      }).catch(e => console.error(e));
    }).catch(e => console.error(e));
  }

  async modalProduk() {
    const modal = await this.modalController.create({
      component: PenjualanProdukPage,
    });

    modal.onDidDismiss().then((data) => {
      this.load_items();
    });

    return await modal.present();
  }

  add_qty(val_id_produk: any, val_kuantiti: any) {
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      let kuantiti = val_kuantiti + 1;

      let query: any = 'UPDATE keranjang SET kuantiti=? WHERE id_produk=?';
      db.executeSql(query, [kuantiti, val_id_produk]).then(async () => {
        console.log('item quantity added');
        this.load_items()
      }).catch(e => console.error(e));
    }).catch(e => console.error(e));
  }

  rem_qty(val_id_produk: any, val_kuantiti: any) {
    if (val_kuantiti > 1) {
      this.sqlite.create({
        name: 'data.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        let kuantiti = val_kuantiti - 1;

        let query: any = 'UPDATE keranjang SET kuantiti=? WHERE id_produk=?';
        db.executeSql(query, [kuantiti, val_id_produk]).then(async () => {
          console.log('item quantity added');
          this.load_items()
        }).catch(e => console.error(e));
      }).catch(e => console.error(e));
    } else {
      this.delete(val_id_produk);
    }
  }

  async delete(val_id_produk: any) {
    const alert = await this.alertController.create({
      header: 'Konfirmasi',
      message: 'Hapus produk dari keranjang ?',
      buttons: [
        {
          text: 'Batal',
          role: 'cancel',
        }, {
          text: 'Hapus',
          handler: () => {
            this.sqlite.create({
              name: 'data.db',
              location: 'default'
            }).then((db: SQLiteObject) => {
              let query: any = 'DELETE FROM keranjang WHERE id_produk=?';
              db.executeSql(query, [val_id_produk]).then(async () => {
                console.log('item deleted');
                this.load_items()
              }).catch(e => console.error(e));
            }).catch(e => console.error(e));
          }
        }
      ]
    });
    await alert.present();
  }

  change() {
    this.kembalian = this.pembayaran - this.total;
  }

  simpan(val_pdf: any) {
    this.tanggal = this.tanggal.substr(0, 4) + "-" + this.tanggal.substr(5, 2) + "-" + this.tanggal.substr(8, 2);

    if (!this.nama_pelanggan) {
      this.nama_pelanggan = 'UMUM';
    }

    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    }).then((db: SQLiteObject) => {

      db.executeSql("SELECT * FROM keranjang", []).then(async (data) => {

        if (data.rows.length == 0) {
          const alert = await this.alertController.create({
            header: 'Info',
            message: 'Silahkan tambah produk.',
            buttons: ['OK']
          });
          await alert.present();
        } else if (!this.pembayaran) {
          const alert = await this.alertController.create({
            header: 'Info',
            message: 'Silahkan isi nominal Pembayaran.',
            buttons: ['OK']
          });
          await alert.present();
        } else if (this.kembalian < 0) {
          const alert = await this.alertController.create({
            header: 'Info',
            message: 'Nominal Pembayaran kurang.',
            buttons: ['OK']
          });
          await alert.present();
        } else {

          //INSERT GLOBAL
          let query: any = 'INSERT INTO transaksi_global ( nota, tanggal, jam, nama_pelanggan, pembayaran ) VALUES ( ?, ?, ?, ?, ?)';
          db.executeSql(query, [this.nota, this.tanggal, this.jam, this.nama_pelanggan, this.pembayaran]).then(async () => {
            console.log('new data global transaction inserted');

            //INSERT DETAIL
            for (let i = 0; i < data.rows.length; i++) {
              this.sqlite.create({
                name: 'data.db',
                location: 'default'
              }).then((db: SQLiteObject) => {

                let query: any = 'INSERT INTO transaksi_detail ( nota, id_produk, harga_pokok, harga_jual, kuantiti ) VALUES ( ?, ?, ?, ?, ?)';
                db.executeSql(query, [this.nota, data.rows.item(i).id_produk, data.rows.item(i).harga_pokok, data.rows.item(i).harga_jual, data.rows.item(i).kuantiti]).then(async () => {
                  console.log('new data detail transaction inserted');

                  let query: any = 'UPDATE produk SET stok=stok-? WHERE id=?';
                  db.executeSql(query, [data.rows.item(i).kuantiti, data.rows.item(i).id_produk]).then(async () => { console.log('stock updated'); }).catch(e => console.error(e));

                  if (i == (data.rows.length - 1)) {
                    this.createPdf(val_pdf); //SIMPAN PDF 
                  }
                }).catch(e => console.error(e));

              }).catch(e => console.error(e));
            }
          }).catch(e => console.error(e));
        }
      }).catch(e => console.error(e));
    }).catch(e => console.error(e));
  }

  createPdf(val_pdf: any) {
    if (!this.nama_pelanggan) {
      this.nama_pelanggan = 'UMUM';
    }

    let items: any = [];

    for (let i = 0; i < this.data.length; i++) {
      items.push([
        { text: this.data[i].nama_produk, fontSize: 9, colSpan: 4 }, {}, {}, {}
      ])

      items.push([
        { text: this.data[i].harga_jual.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."), fontSize: 9 },
        { text: ' x ' + this.data[i].kuantiti, fontSize: 9 },
        { text: ':', fontSize: 9 },
        { text: (this.data[i].harga_jual * this.data[i].kuantiti).toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."), style: 'nominal' }
      ])
    }

    items.push([
      { text: ' ', fontSize: 4 }, {}, {}, {}
    ])

    items.push([
      { text: 'Total', style: 'total', colSpan: 2 },
      {},
      { text: ':', fontSize: 9 },
      { text: this.total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."), style: 'nominal' }
    ])

    items.push([
      { text: 'Bayar', style: 'total', colSpan: 2 },
      {},
      { text: ':', fontSize: 9 },
      { text: this.pembayaran.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."), style: 'nominal' }
    ])

    items.push([
      { text: 'Kembali', style: 'total', colSpan: 2 },
      {},
      { text: ':', fontSize: 9 },
      { text: this.kembalian.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."), style: 'nominal' }
    ])

    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    }).then((db: SQLiteObject) => {

      let query: any = 'SELECT * FROM akun';
      db.executeSql(query, []).then(async (data) => {
        var docDefinition: any = {
          pageMargins: [5, 5, 5, 20],
          pageSize: {
            width: 141.00,
            //  width: 300.00,
            height: 'auto'
          },
          content: [
            { text: data.rows.item(0).nama_usaha, style: 'header' },
            {
              layout: 'noBorders',
              table: {
                headerRows: 1,
                widths: ['*', 'auto'],

                body: [
                  [
                    { text: 'Nota :', style: 'subheader' },
                    { text: 'Waktu :', style: 'subheader' }
                  ],
                  [
                    { text: this.nota, fontSize: 9 },
                    { text: this.tanggal + ' ' + this.jam, fontSize: 9 }
                  ],
                ]
              }
            },

            // { text: 'Nama Pelanggan :', style: 'subheader' },
            // { text: this.nama_pelanggan, fontSize: 9 },

            { text: ' ', fontSize: 9 },

            //ITEMS
            {
              // layout: 'noBorders',
              layout: {
                hLineWidth: function (i: any, node: any) {
                  return (i === 0 || i === node.table.body.length - 3) ? 1 : 0;
                },
                vLineWidth: function (i: any, node: any) {
                  return 0;
                }
              },


              table: {
                headerRows: 1,
                widths: ['auto', '*', 'auto', 'auto'],
                body: items,
              }
            },
            { text: 'Terimakasih', style: 'footer' },

          ],
          styles: {
            header: {
              alignment: 'center',
              fontSize: 11,
              bold: true,
              marginBottom: 8,
            },
            subheader: {
              fontSize: 9,
              bold: true,
              marginTop: 4,
            },
            nominal: {
              fontSize: 9,
              alignment: 'right',
            },
            total: {
              fontSize: 9,
            },
            footer: {
              alignment: 'center',
              fontSize: 9,
              bold: true,
              marginTop: 16,
              marginBottom: 16,
            },
          }
        }
        this.pdfObj = pdfMake.createPdf(docDefinition);

        this.pdfObj.getBuffer((buffer: any) => {
          var blob = new Blob([buffer], { type: 'application/pdf' });

          // Save the PDF to the data Directory of our App
          this.file.writeFile(this.file.dataDirectory, 'Nota ' + this.nota, blob, { replace: true }).then(fileEntry => {
            // Open the PDf with the correct OS tools
            if (val_pdf) {
              this.fileOpener.open(this.file.dataDirectory + 'Nota ' + this.nota, 'application/pdf');
            }

            this.sqlite.create({
              name: 'data.db',
              location: 'default'
            }).then((db: SQLiteObject) => {
              let query: any = 'DELETE FROM keranjang';
              db.executeSql(query, []).then(async () => { console.log('table truncate'); }).catch(e => console.error(e));
            }).catch(e => console.error(e));

            this.ionViewDidEnter();
          })
        });

        const toast = await this.toastController.create({
          message: 'Transaksi berhasil tersimpan.\n\nSilahkan membuat transaksi baru.',
          position: 'top',
          color: 'success',
          duration: 2000,
          buttons: [
            {
              text: 'X',
              role: 'cancel',
              handler: () => {
                console.log('Cancel clicked');
              }
            }
          ]
        });
        toast.present();



      }).catch(e => console.error(e));
    }).catch(e => console.error(e));
  }

}
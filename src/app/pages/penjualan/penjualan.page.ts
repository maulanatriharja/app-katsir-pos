import { SQLite, SQLiteObject } from "@awesome-cordova-plugins/sqlite/ngx";
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-penjualan',
  templateUrl: './penjualan.page.html',
  styleUrls: ['./penjualan.page.scss'],
})
export class PenjualanPage implements OnInit {

  data: any = [];
  empty: boolean = true;

  constructor(
    public sqlite: SQLite,
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      let query: any = 'SELECT * '
        + 'FROM transaksi_global AS t1 '
        + 'LEFT JOIN (SELECT nota, SUM(harga_jual*kuantiti) AS total FROM transaksi_detail GROUP BY nota) AS t2 ON t1.nota=t2.nota';
      db.executeSql(query, []).then((data) => {
        console.log('list data showed');

        this.data = [];

        if (data.rows.length > 0) {
          this.empty = false;

          for (let i = 0; i < data.rows.length; i++) {
            this.data.push(data.rows.item(i));
          }
        } else {
          this.empty = true;
        }
      }).catch(e => console.error(e));
    }).catch(e => console.error(e));
  }
}
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PenjualanDetailPage } from './penjualan-detail.page';

const routes: Routes = [
  {
    path: '',
    component: PenjualanDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PenjualanDetailPageRoutingModule {}

import { AlertController, ModalController, ToastController } from '@ionic/angular';
import { SQLite, SQLiteObject } from "@awesome-cordova-plugins/sqlite/ngx";
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from "@angular/common";

import { PenjualanProdukPage } from '../penjualan-produk/penjualan-produk.page';

import * as moment from 'moment';

@Component({
  selector: 'app-penjualan-detail',
  templateUrl: './penjualan-detail.page.html',
  styleUrls: ['./penjualan-detail.page.scss'],
})
export class PenjualanDetailPage implements OnInit {

  data: any = [];
  items: any = [];
  total: number = 0;
  pembayaran: number = 0;
  kembalian: number = 0;
  empty: boolean = true;

  constructor(
    public alertController: AlertController,
    public location: Location,
    public modalController: ModalController,
    public route: ActivatedRoute,
    public sqlite: SQLite,
    public toastController: ToastController,
  ) { }

  nota: string = this.route.snapshot.paramMap.get('nota')!;

  ngOnInit() {

  }

  ionViewWillEnter() {
    this.load_data();
  }

  load_data() {
    //GLOBAL
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      let query: any = 'SELECT * FROM transaksi_global WHERE nota=?';
      db.executeSql(query, [this.nota]).then((data) => {
        console.log(JSON.stringify(data.rows.item(0)));

        this.data = data.rows.item(0);
      }).catch(e => console.error(e));
    }).catch(e => console.error(e));

    //DETAIL
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      let query: any = 'SELECT t1.*, t2.nama_produk FROM transaksi_detail t1 LEFT JOIN produk t2 ON t1.id_produk = t2.id WHERE nota=?';
      db.executeSql(query, [this.nota]).then((data) => {
        console.log('list data showed');

        this.items = [];
        this.total = 0;

        if (data.rows.length > 0) {
          this.empty = false;

          for (let i = 0; i < data.rows.length; i++) {
            this.items.push(data.rows.item(i));

            this.total += (data.rows.item(i).harga_jual * data.rows.item(i).kuantiti);
          }
        } else {
          this.empty = true;
        }
      }).catch(e => console.error(e));
    }).catch(e => console.error(e));
  }

}
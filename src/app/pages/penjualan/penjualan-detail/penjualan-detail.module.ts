import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PenjualanDetailPageRoutingModule } from './penjualan-detail-routing.module';

import { PenjualanDetailPage } from './penjualan-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PenjualanDetailPageRoutingModule
  ],
  declarations: [PenjualanDetailPage]
})
export class PenjualanDetailPageModule {}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PenjualanProdukPage } from './penjualan-produk.page';

const routes: Routes = [
  {
    path: '',
    component: PenjualanProdukPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PenjualanProdukPageRoutingModule {}

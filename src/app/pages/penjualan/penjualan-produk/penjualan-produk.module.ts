import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PenjualanProdukPageRoutingModule } from './penjualan-produk-routing.module';

import { PenjualanProdukPage } from './penjualan-produk.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PenjualanProdukPageRoutingModule
  ],
  declarations: [PenjualanProdukPage]
})
export class PenjualanProdukPageModule {}

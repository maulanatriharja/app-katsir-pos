import { SQLite, SQLiteObject } from "@awesome-cordova-plugins/sqlite/ngx";
import { AdMobPro } from '@awesome-cordova-plugins/admob-pro/ngx';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-penjualan-produk',
  templateUrl: './penjualan-produk.page.html',
  styleUrls: ['./penjualan-produk.page.scss'],
})
export class PenjualanProdukPage implements OnInit {

  data: any = [];
  empty: boolean = true;

  constructor(
    public admob: AdMobPro,
    public modalController: ModalController,
    public sqlite: SQLite,
  ) { }

  ngOnInit() {
  }

  inputFocus() {
    this.admob.hideBanner();
  }

  inputBlur() {
    this.admob.showBanner(this.admob.AD_POSITION.BOTTOM_CENTER);
  }

  ionViewDidEnter(event: any) {
    let pencarian: string = '';

    if (event) { pencarian = event.target.value }

    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      let query: any = 'SELECT * FROM produk WHERE nama_produk LIKE ?';
      db.executeSql(query, ['%' + pencarian + '%']).then((data) => {
        console.log('list data showed');

        this.data = [];

        if (data.rows.length > 0) {
          this.empty = false;

          for (let i = 0; i < data.rows.length; i++) {
            this.data.push(data.rows.item(i));
          }
        } else {
          this.empty = true;
        }
      }).catch(e => console.error(e));
    }).catch(e => console.error(e));
  }

  tambah(val_id_produk: any, val_harga_pokok: any, val_harga_jual: any) {
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    }).then((db: SQLiteObject) => {

      let query_cek: any = "SELECT * FROM keranjang WHERE id_produk=?";
      db.executeSql(query_cek, [val_id_produk]).then((data) => {
        console.log('list data showed');

        if (data.rows.length > 0) {
          let kuantiti = data.rows.item(0).kuantiti + 1;

          let query: any = 'UPDATE keranjang SET kuantiti=? WHERE id=?';
          db.executeSql(query, [kuantiti, val_id_produk]).then(async () => {
            console.log('item quantity added');
            this.modalController.dismiss();
          }).catch(e => console.error(e));
        } else {
          let query: any = 'INSERT INTO keranjang ( id_produk, harga_pokok, harga_jual, kuantiti ) VALUES ( ?, ?, ?, ?)';
          db.executeSql(query, [val_id_produk, val_harga_pokok, val_harga_jual, 1]).then(() => {
            console.log('new data inserted');
            this.modalController.dismiss();
          }).catch(e => console.error(e));
        }

      }).catch(e => console.error(e));
    }).catch(e => console.error(e));
  }
}

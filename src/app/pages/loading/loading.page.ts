import { SQLite, SQLiteObject } from "@awesome-cordova-plugins/sqlite/ngx";
import { NavController, Platform } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.page.html',
  styleUrls: ['./loading.page.scss'],
})
export class LoadingPage implements OnInit {

  constructor(
    public navCtrl: NavController,
    public platform: Platform,
    public router: Router,
    public sqlite: SQLite,
    public storage: Storage,
  ) { }

  async ngOnInit() {

  }

  ionViewDidEnter() {

    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'data.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        let query: any = 'SELECT * FROM akun';
        db.executeSql(query, []).then(async (data) => {
          this.storage.get('loggedin').then((val) => {
            if (val == true) {
              // this.navCtrl.navigateRoot('/produk');
              this.router.navigate(['/produk'], { replaceUrl: true });
            } else {
              // this.navCtrl.navigateRoot('/login');
              this.router.navigate(['/login'], { replaceUrl: true });
            }
          });
        }).catch(e => {
          console.error(e);
          // this.navCtrl.navigateRoot('/registrasi');
          this.router.navigate(['/registrasi'], { replaceUrl: true });
        });
      }).catch(e => console.error(e));
    });

  }

}

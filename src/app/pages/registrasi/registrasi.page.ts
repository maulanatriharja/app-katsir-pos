import { SQLite, SQLiteObject } from "@awesome-cordova-plugins/sqlite/ngx";
import { FormGroup, FormBuilder } from "@angular/forms";
import { AppComponent } from '../../app.component';
import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registrasi',
  templateUrl: './registrasi.page.html',
  styleUrls: ['./registrasi.page.scss'],
})
export class RegistrasiPage implements OnInit {

  myForm: FormGroup;
  isSubmitted = false;

  constructor(
    public appComponent: AppComponent,
    public formBuilder: FormBuilder,
    public loadingController: LoadingController,
    public navCtrl: NavController,
    public router: Router,
    public storage: Storage,
    public sqlite: SQLite,
  ) {
    this.myForm = this.formBuilder.group({
      nama_usaha: [''],
      nama_user: [''],
      password: [''],
      password_konf: [''],
    });
  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    // this.sqlite.create({
    //   name: 'data.db',
    //   location: 'default'
    // }).then((db: SQLiteObject) => {


    //   let query: any = 'SELECT * FROM akun';
    //   db.executeSql(query, []).then(async (data) => {
    //     if (data.rows.length > 0) {
    //       // this.router.navigate(['/login'], { replaceUrl: true });
    //       this.navCtrl.navigateRoot('/produk');
    //       // this.appComponent.pubLogin({}); 
    //     }
    //   }).catch(e => console.error(e));
    // }).catch(e => console.error(e));
  }

  async submit() {
    this.isSubmitted = true;

    if (this.myForm.valid && this.myForm.get('password')!.value == this.myForm.get('password_konf')!.value) {

      const loading = await this.loadingController.create({});
      await loading.present();

      this.sqlite.create({
        name: 'data.db',
        location: 'default'
      }).then((db: SQLiteObject) => {

        let query_akun = 'CREATE TABLE IF NOT EXISTS akun( id INTEGER PRIMARY KEY, nama_usaha, nama_user, password)';
        db.executeSql(query_akun, []).then(() => {
          console.log('new table akun created');

          let query: any = 'INSERT INTO akun ( nama_usaha, nama_user, password ) VALUES ( ?, ?, ? )';
          db.executeSql(query, [
            this.myForm.get('nama_usaha')!.value,
            this.myForm.get('nama_user')!.value,
            this.myForm.get('password')!.value,
          ]).then(async () => {
            console.log('new data inserted');

            this.appComponent.pubAkun({});
            this.router.navigate(['/produk'], { replaceUrl: true });
            this.storage.set('loggedin', true);

            loading.dismiss();

          }).catch(e => { console.error(e); loading.dismiss(); });
        }).catch(e => { console.error(e); loading.dismiss(); });
      }).catch(e => { console.error(e); loading.dismiss(); });

    }
  }

}

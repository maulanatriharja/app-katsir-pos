import { AlertController, Platform, ToastController } from '@ionic/angular';
import { SQLite, SQLiteObject } from "@awesome-cordova-plugins/sqlite/ngx";
import { FormGroup, FormBuilder } from "@angular/forms";
import { AdMobPro } from '@awesome-cordova-plugins/admob-pro/ngx';
import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Location } from "@angular/common";
import { Storage } from '@ionic/storage-angular';

@Component({
  selector: 'app-setting-password',
  templateUrl: './setting-password.page.html',
  styleUrls: ['./setting-password.page.scss'],
})
export class SettingPasswordPage implements OnInit {

  myForm: FormGroup;
  isSubmitted = false;

  constructor(
    public admob: AdMobPro,
    public alertController: AlertController,
    public formBuilder: FormBuilder,
    public location: Location,
    public navCtrl: NavController,
    public platform: Platform,
    public storage: Storage,
    public sqlite: SQLite,
    public toastController: ToastController,
  ) {
    this.myForm = this.formBuilder.group({
      password_lama: [''],
      password_baru: [''],
      password_konf: [''],
    });
  }

  ngOnInit() {
  }

  inputFocus() {
    this.admob.hideBanner();
  }

  inputBlur() {
    this.admob.showBanner(this.admob.AD_POSITION.BOTTOM_CENTER);
  }


  submit() {
    this.isSubmitted = true;

    if (this.myForm.valid && this.myForm.get('password_baru')!.value == this.myForm.get('password_konf')!.value) {
      this.sqlite.create({
        name: 'data.db',
        location: 'default'
      }).then((db: SQLiteObject) => {

        let query_akun = 'SELECT password FROM akun';
        db.executeSql(query_akun, []).then((data) => {

          if (this.myForm.get('password_lama')!.value != data.rows.item(0).password) {
            alert("password lama salah");
          } else {
            let query: any = 'UPDATE akun SET password=?';
            db.executeSql(query, [this.myForm.get('password_baru')!.value]).then(async () => {
              console.log('item quantity added');

              const toast = await this.toastController.create({
                message: 'Berhasil mengubah password.',
                position: 'top',
                color: 'light',
                duration: 2000,
                buttons: [
                  {
                    text: 'X',
                    role: 'cancel',
                    handler: () => {
                      console.log('Cancel clicked');
                    }
                  }
                ]
              });
              toast.present();

              this.location.back();
            }).catch(e => console.error(e));
          }


        }).catch(e => console.error(e));
      }).catch(e => console.error(e));

    }
  }

  ionViewDidLeave() {
    this.inputBlur();
  }

}
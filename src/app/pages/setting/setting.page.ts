import { SQLite, SQLiteObject } from "@awesome-cordova-plugins/sqlite/ngx";
import { AlertController, Platform } from '@ionic/angular';
import { AdMobPro } from '@awesome-cordova-plugins/admob-pro/ngx';
import { AppComponent } from '../../app.component';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.page.html',
  styleUrls: ['./setting.page.scss'],
})
export class SettingPage implements OnInit {

  nama_usaha!: string;
  nama_user!: string;
  password!: string;

  constructor(
    public admob: AdMobPro,
    public alertController: AlertController,
    public appComponent: AppComponent,
    public platform: Platform,
    public sqlite: SQLite,
  ) {

    this.appComponent.obvAkun().subscribe(() => {
      this.ionViewDidEnter();
    });
  }

  ngOnInit() {
  }

  inputFocus() {
    this.admob.hideBanner();
  }

  inputBlur() {
    this.admob.showBanner(this.admob.AD_POSITION.BOTTOM_CENTER);
  }

  ionViewDidEnter() {
    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'data.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        let query: any = 'SELECT * FROM akun';
        db.executeSql(query, []).then(async (data) => {
          if (data.rows.length > 0) {
            this.nama_usaha = data.rows.item(0).nama_usaha;
            this.nama_user = data.rows.item(0).nama_user;
          }
        }).catch(e => console.error(e));
      }).catch(e => console.error(e));
    });
  }

  async ubah_usaha() {
    this.inputFocus();

    const alert = await this.alertController.create({
      header: 'Ubah Nama Usaha',
      inputs: [
        {
          name: 'nama_usaha',
          type: 'text',
          value: this.nama_usaha,
          placeholder: 'Nama Usaha',
          cssClass: 'alert-custom-css',
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
            this.inputBlur();
          }
        }, {
          text: 'Ok',
          handler: (data) => {
            console.log('Confirm Ok');

            this.sqlite.create({
              name: 'data.db',
              location: 'default'
            }).then((db: SQLiteObject) => {
              let query: any = 'UPDATE akun SET nama_usaha=?';
              db.executeSql(query, [data.nama_usaha]).then(async () => {
                console.log('data updated');

                this.appComponent.pubAkun({});
                this.inputBlur();
              }).catch(e => console.error(e));
            }).catch(e => console.error(e));
          }
        }
      ]
    });
    await alert.present();
  }

  async ubah_user() {
    this.inputFocus();

    const alert = await this.alertController.create({
      header: 'Ubah Nama User',
      inputs: [
        {
          name: 'nama_user',
          type: 'text',
          value: this.nama_user,
          placeholder: 'Nama User',
          cssClass: 'alert-custom-css',
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
            this.inputBlur();
          }
        }, {
          text: 'Ok',
          handler: (data) => {
            console.log('Confirm Ok');

            this.sqlite.create({
              name: 'data.db',
              location: 'default'
            }).then((db: SQLiteObject) => {
              let query: any = 'UPDATE akun SET nama_user=?';
              db.executeSql(query, [data.nama_user]).then(async () => {
                console.log('data updated');

                this.appComponent.pubAkun({});
                this.inputBlur();
              }).catch(e => console.error(e));
            }).catch(e => console.error(e));
          }
        }
      ]
    });
    await alert.present();
  }

}

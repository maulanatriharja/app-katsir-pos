import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    // redirectTo: 'registrasi',
    redirectTo: 'loading',
    pathMatch: 'full'
  },
  {
    path: 'loading',
    loadChildren: () => import('./pages/loading/loading.module').then(m => m.LoadingPageModule)
  },
  {
    path: 'penjualan',
    loadChildren: () => import('./pages/penjualan/penjualan.module').then(m => m.PenjualanPageModule)
  },
  {
    path: 'penjualan-create',
    loadChildren: () => import('./pages/penjualan/penjualan-create/penjualan-create.module').then(m => m.PenjualanCreatePageModule)
  },
  {
    path: 'penjualan-detail/:nota',
    loadChildren: () => import('./pages/penjualan/penjualan-detail/penjualan-detail.module').then(m => m.PenjualanDetailPageModule)
  },
  {
    path: 'penjualan-produk',
    loadChildren: () => import('./pages/penjualan/penjualan-produk/penjualan-produk.module').then(m => m.PenjualanProdukPageModule)
  },
  {
    path: 'penjualan-riwayat',
    loadChildren: () => import('./pages/penjualan-riwayat/penjualan-riwayat.module').then(m => m.PenjualanRiwayatPageModule)
  },
  {
    path: 'pengeluaran',
    loadChildren: () => import('./pages/pengeluaran/pengeluaran.module').then(m => m.PengeluaranPageModule)
  },
  {
    path: 'pengeluaran-detail',
    loadChildren: () => import('./pages/pengeluaran/pengeluaran-detail/pengeluaran-detail.module').then(m => m.PengeluaranDetailPageModule)
  },
  {
    path: 'produk',
    loadChildren: () => import('./pages/produk/produk.module').then(m => m.ProdukPageModule)
  },
  {
    path: 'produk-detail/:id',
    loadChildren: () => import('./pages/produk/produk-detail/produk-detail.module').then(m => m.ProdukDetailPageModule)
  },
  {
    path: 'pelanggan',
    loadChildren: () => import('./pages/pelanggan/pelanggan.module').then(m => m.PelangganPageModule)
  },
  {
    path: 'pelanggan-detail',
    loadChildren: () => import('./pages/pelanggan/pelanggan-detail/pelanggan-detail.module').then(m => m.PelangganDetailPageModule)
  },
  {
    path: 'laporan',
    loadChildren: () => import('./pages/laporan/laporan.module').then(m => m.LaporanPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: 'registrasi',
    loadChildren: () => import('./pages/registrasi/registrasi.module').then(m => m.RegistrasiPageModule)
  },
  {
    path: 'akun',
    loadChildren: () => import('./pages/akun/akun.module').then(m => m.AkunPageModule)
  },
  {
    path: 'setting',
    loadChildren: () => import('./pages/setting/setting.module').then(m => m.SettingPageModule)
  },
  {
    path: 'setting-password',
    loadChildren: () => import('./pages/setting/setting-password/setting-password.module').then(m => m.SettingPasswordPageModule)
  },
  {
    path: 'info',
    loadChildren: () => import('./pages/info/info.module').then(m => m.InfoPageModule)
  },
  {
    path: 'laporan-penjualan',
    loadChildren: () => import('./pages/laporan-penjualan/laporan-penjualan.module').then(m => m.LaporanPenjualanPageModule)
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

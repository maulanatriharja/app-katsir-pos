import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@awesome-cordova-plugins/splash-screen/ngx';
import { StatusBar } from '@awesome-cordova-plugins/status-bar/ngx';

import { AppComponent, } from './app.component';
import { AppRoutingModule } from './app-routing.module';

// import { Camera } from '@awesome-cordova-plugins/camera/ngx';
import { IonicStorageModule } from '@ionic/storage-angular';
import { SQLite } from '@awesome-cordova-plugins/sqlite/ngx';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdMobPro } from '@awesome-cordova-plugins/admob-pro/ngx';
import { AppVersion } from '@awesome-cordova-plugins/app-version/ngx';
// import { PDFGenerator } from '@awesome-cordova-plugins/pdf-generator';
import { File } from '@awesome-cordova-plugins/file/ngx';
import { FileOpener } from '@awesome-cordova-plugins/file-opener/ngx';
import { FilePath } from '@awesome-cordova-plugins/file-path/ngx';
import { Keyboard } from '@awesome-cordova-plugins/keyboard/ngx';

import { registerLocaleData } from '@angular/common';
registerLocaleData(localeId);
import localeId from '@angular/common/locales/id';

@NgModule({
  declarations: [AppComponent],
  // entryComponents: [],
  imports: [
    AppRoutingModule,
    BrowserModule,
    FormsModule,
    IonicModule.forRoot({
      backButtonText: '',
      backButtonIcon: 'arrow-back',
      mode: 'ios'
    }),
    IonicStorageModule.forRoot(),
    ReactiveFormsModule,
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'id' },
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    AdMobPro,
    AppVersion,
    // Camera,
    File,
    FileOpener,
    FilePath,
    // PDFGenerator,
    Keyboard,
    SplashScreen,
    SQLite,
    StatusBar,
  ],
  bootstrap: [AppComponent],
  schemas: []
})
export class AppModule { }

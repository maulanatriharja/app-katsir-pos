import { AlertController, NavController, Platform } from '@ionic/angular';
import { SQLite, SQLiteObject } from "@awesome-cordova-plugins/sqlite/ngx";
import { SplashScreen } from '@awesome-cordova-plugins/splash-screen/ngx';
import { StatusBar } from '@awesome-cordova-plugins/status-bar/ngx';
import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {

  nama_usaha!: string;
  nama_user!: string;

  private akun = new Subject<any>();
  pubAkun(data: any) { this.akun.next(data); }
  obvAkun(): Subject<any> { return this.akun; }

  public selectedIndex = 0;

  constructor(
    public alertController: AlertController,
    public navCtrl: NavController,
    public platform: Platform,
    public splashScreen: SplashScreen,
    public sqlite: SQLite,
    public statusBar: StatusBar,
    public storage: Storage,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.backgroundColorByHexString('#3880ff');
      this.statusBar.overlaysWebView(false);

      this.splashScreen.hide();

      this.loadAkun();
      this.obvAkun().subscribe(() => {
        this.loadAkun();
      });
    });
  }

  async ngOnInit() {
    await this.storage.create();
  }

  loadAkun() {
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      let query: any = 'SELECT * FROM akun';
      db.executeSql(query, []).then(async (data) => {
        if (data.rows.length > 0) {
          this.nama_usaha = data.rows.item(0).nama_usaha;
          this.nama_user = data.rows.item(0).nama_user;
        } else {
          this.navCtrl.navigateRoot('/login');
        }
      }).catch(e => console.error(e));
    }).catch(e => console.error(e));
  }

  checkProduct() {
    //CEK IS PRODUCT NULL
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      let query: any = 'SELECT * FROM produk';
      db.executeSql(query, []).then(async (data) => {


        if (data.rows.length == 0) {
          const alert = await this.alertController.create({
            header: 'Info',
            message: 'Silahkan menambahkan produk baru di halaman Data Produk dahulu.',
            buttons: [{
              text: 'OK',
              handler: () => {
                this.navCtrl.navigateRoot('/produk');
                this.selectedIndex = 0;
              }
            }]
          });
          await alert.present();
        } else {
          this.navCtrl.navigateRoot('/penjualan-create');
        }

      }).catch(e => console.error(e));
    }).catch(e => console.error(e));
  }

  keluar() {
    this.storage.set('loggedin', false).then(() => {
      this.navCtrl.navigateRoot('/login');
    });
  }
}

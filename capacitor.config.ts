import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'maulana.mantapp.katsir',
  appName: 'Katsir POS',
  webDir: 'www',
  server: {
    androidScheme: 'https'
  }
};

export default config;
